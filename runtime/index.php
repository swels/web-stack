<?php 

require_once(__DIR__.'/config.php');
require_once(ROOT_DIR.'/vendor/autoload.php');
require_once(ROOT_DIR.'/runtime/Runtime.php');

if(!function_exists('dd')) {
    function dd() {
        array_map(function($x) { var_dump($x); }, func_get_args()); die;
    }
}

if(!function_exists('rt')) {
	function rt() {
	    return Runtime::getInstance();
	}
}
