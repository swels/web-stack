<?php
namespace Runtime\Controller;

use PDO;

class LoginController {

    public function install() {

        if (!rt()->db()->query('CREATE DATABASE IF NOT EXISTS '.ENV['DB_NAME'])) {
            printf("Errormessage: %s\n", rt()->db()->error);
            die();
        }

        rt()->db()->query('
            CREATE TABLE IF NOT EXISTS `'.ENV['DB_NAME'].'`.`users` 
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `first_name` VARCHAR(50) NOT NULL,
                `last_name` VARCHAR(50) NOT NULL,
                `user_name` VARCHAR(50) NOT NULL,
                `email` VARCHAR(100) NOT NULL,
                `password` VARCHAR(255) NOT NULL,
                `active` BOOL NOT NULL DEFAULT 0,
                `group_id` INT NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`) 
            ) CHARSET=UTF8;') or die(rt()->db()->error);
    }

    public function loginUser() : bool {
        $stmt = rt()->pdo()->prepare('select * from users');
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        dd($result);
        return false;
        /*
        $success = false;
         try{
            $con = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); 
            $con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql = "SELECT * FROM users WHERE username = :username AND password = :password LIMIT 1";
                        $user = username;

            $stmt = $con->
            $stmt->bindValue( "username", $this->username, PDO::PARAM_STR );
            $stmt->bindValue( "password", md5($this->password), PDO::PARAM_STR );
            $stmt->execute();

            $valid = $stmt->fetchColumn();

            if( $valid ) {
            $success = true;
                        session_start();


            session_regenerate_id();
            $_SESSION['user'] = $user['user'];
            session_write_close();
            echo ('Login');
            exit();

            }

            $con = null;
            return $success;
            }catch (PDOException $e) {
            echo $e->getMessage();
            return $success;
         }*/
    }

    public function registerUser() : bool
    {
        $rules = [
            'first_name' => '#^[a-zA-Z -]{2,}$#',
            'last_name' => '#^[a-zA-Z -]{2,}$#',
            'user_name' => '#^[\w -]{3,}$#',
            'email' => 'FILTER_VALIDATE_EMAIL',
            'email_confirm' => 'FILTER_CONFIRM:email',
            'password' => '#.{6,}#',
            'password_confirm' => 'FILTER_CONFIRM:password',
        ];

        $postData = $this->validatePOST($rules);
        if(empty($_POST) || count($_POST) !== count($rules)) {
            $_POST['message'] = 'invalid request!';
            return false;
        }

        if ($postData['allValid'] === true && $stmt = rt()->db()->prepare('INSERT INTO `'.ENV['DB_NAME'].'`.`users` 
                    (first_name, last_name, user_name, email, password, active, group_id) 
                    VALUES (?, ?, ?, ?, ?, ?, ?)')) {

            $firstname = rt()->db()->escape_string($_POST['first_name']);
            $lastname = rt()->db()->escape_string($_POST['last_name']);
            $username = rt()->db()->escape_string($_POST['user_name']);
            $email = rt()->db()->escape_string($_POST['email']);
            $password = rt()->db()->escape_string(password_hash($_POST['password'], PASSWORD_BCRYPT));

            // TODO
            $active = 1;
            $group = 1;

            if ($stmt->bind_param("sssssii", $firstname, $lastname, $username, $email, $password, $active, $group) === false) {
              die('bind_param() failed: ' . htmlspecialchars($stmt->error));
            }

            if ($stmt->execute() === false) {
              die('execute() failed: ' . htmlspecialchars($stmt->error));
            }
            
            $stmt->close();
            return true;
        }
        elseif (!$postData['first_name']) {
            $_POST['message'] = 'Invalid input for first name!'; 
        }
        else if (!$postData['last_name']) {
            $_POST['message'] = 'Invalid input for last name!'; 
        }
        else if (!$postData['user_name']) {
            $_POST['message'] = 'Invalid input for user name!'; 
        }
        else if (!$postData['email']) {
            $_POST['message'] = 'Invalid input for email!';
        }
        else if (!$postData['email_confirm']) {
            $_POST['message'] = 'Email does not match with email-confirm field!';
            $_POST['email'] = $_POST['email_confirm'] = '';
        }
        elseif (!$postData['password']) {
            $_POST['message'] = 'The password is to short!';
            $_POST['password'] = $_POST['password_confirm'] = '';
        }
        else if (!$postData['password_confirm']) {
            $_POST['message'] = 'Password does not match with password-confirm field!';
            $_POST['password'] = $_POST['password_confirm'] = '';
        }
        else {
            $_POST['message'] = 'sorry - an error occurred while creating a new user';
        }

        return false;
    }


    private function validatePOST($rules)
    {
        $result = [
            'allValid' => true,
        ];

        foreach ($rules as $attribute => $rule) {

            $result[$attribute] = false;

            if(isset($_POST[$attribute])) {
                if(preg_match('#FILTER_VALIDATE_#', $rule) && filter_var($_POST[$attribute], constant($rule))) {
                    $result[$attribute] = true;
                }
                elseif(preg_match('#FILTER_CONFIRM:.#', $rule)) {
                    $confirm_attr = explode(':', $rule)[1];
                    $result[$attribute] = isset($_POST[$confirm_attr]) && $_POST[$attribute] === $_POST[$confirm_attr];
                }
                elseif(preg_match($rule, $_POST[$attribute])) {
                    $result[$attribute] = true;
                }
            }
            
            if($result[$attribute] === false) {
                $result['allValid'] = false;
            }
        }
        return $result;
    }
}
