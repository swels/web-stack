<?php

define('ROOT_DIR', dirname( dirname(__FILE__) ));
define('ENV', parse_ini_file(ROOT_DIR.DIRECTORY_SEPARATOR.'.env'));
define('TEMPLATES_DIR', ROOT_DIR.ENV['TEMPLATES_DIR']);

if(ENV['DEBUG'] == 1) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}
