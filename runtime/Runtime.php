<?php 

use Runtime\Controller\LoginController;

class Runtime {

    protected static $instance = null;

    private $pdo;
    private $dbConn;
    private $cachebustTs;

    public $loginController;

    function __construct(){
        $this->loginController = new LoginController();
    }

    public static function getInstance() {
        if(self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function db() {
        if(empty($this->dbConn)) {
            $this->dbConn = new mysqli(ENV['DB_HOST'], ENV['DB_USER'], ENV['DB_PASSWORD'], ENV['DB_NAME']);
        }

        if ($this->dbConn->connect_errno) {
            printf("Connection failed: %s\n", $mysqli->connect_error);
            die();
        }
        return $this->dbConn;
    }

    public function pdo() {
        if(empty($this->pdo)) {
            try {
                $db_dsn = ENV['PDO_DRIVER'].':dbname='.ENV['DB_NAME'].';host='.ENV['DB_HOST'];
                $this->pdo = new PDO($db_dsn, ENV['DB_USER'], ENV['DB_PASSWORD']);
                $this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                die('Connection failed: ' . $e->getMessage());
            }
        }
        return $this->pdo;
    }

    public function cachebust() {

        if(empty($this->cachebustTs)) {
            $this->cachebustTs = '?ts='.time();

            $cachebustFile = ROOT_DIR.DIRECTORY_SEPARATOR.'cachebust.ini';
            if(file_exists($cachebustFile)) {
                $this->cachebustTs = '?ts='.file_get_contents($cachebustFile);
            }
        }

        echo($this->cachebustTs);
    }

    public function template(string $template, $data = []) {
        try {
            $viewFile = TEMPLATES_DIR.DIRECTORY_SEPARATOR.strtolower($template).'.php';

            if (file_exists($viewFile)) {
                extract($data);
                $runtime = $this;
                include($viewFile);
            } else {
                throw new RuntimeException('Template ' . $viewFile . ' not found!');
            }
        }
        catch (customException $e) {
            echo $e->errorMessage();
        }
    }

    public function isLoggedIn() {
        return false;
    }
}
