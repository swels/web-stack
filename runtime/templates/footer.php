
</div>

<footer>
  <div id="footer-bar" class="container">
    <div class="grid full-width">
      <div class="col-12 full-width no-padding">
        <span class="copyright">Copyright &copy; <?php echo(date('Y')); ?></span>
        <div class="logo">
          
        </div>
      </div>
    </div>
  </div>
</footer>

<script src="/assets/dist/main.js<?php $runtime->cachebust();?>"></script>
</body>
</html>