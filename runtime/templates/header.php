<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="web and electronic projects" />

  <title>Web-Stack</title>
  <link rel="shortcut icon" href="/assets/icons/favicon.ico" />

  <link rel="stylesheet" href="/assets/dist/libs.css<?php $runtime->cachebust();?>" />
  <link rel="stylesheet" href="/assets/dist/index.css<?php $runtime->cachebust();?>" />

  <link rel="stylesheet" media="(min-width: 0) AND (max-width: 568px)" href="/assets/dist/indexXs.css<?php $runtime->cachebust();?>" />
  <link rel="stylesheet" media="(min-width: 569px) AND (max-width: 768px)" href="/assets/dist/indexSm.css<?php $runtime->cachebust();?>" />
  <link rel="stylesheet" media="(min-width: 769px) AND (max-width: 1024px)" href="/assets/dist/indexMd.css<?php $runtime->cachebust();?>" />

  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans" rel="stylesheet" />
  <script src="/assets/dist/libs.js<?php $runtime->cachebust();?>"></script>
</head>
<body>

<header>
  <nav id="nav-bar" class="container">
    <ul>
      <li class="active" >
        <a href="/">Home</a>
      </li>
    </ul>
  </nav>
</header>

<div id="main" class="section">
