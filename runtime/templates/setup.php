<h1><?php echo($message ?? 'Setup'); ?></h1>
<form method="post">
    <h6>First name</h6>
    <input type="text" name="first_name" required pattern="^[a-zA-Z -]{2,}$" title="2-50 chars: a-z, A-Z, -" value="<?php echo($first_name ?? ''); ?>">
    <h6>Last name</h6>
    <input type="text" name="last_name" required pattern="^[a-zA-Z -]{2,}$" title="2-50 chars: a-z, A-Z, -" value="<?php echo($last_name ?? ''); ?>">
    <h6>User name</h6>
    <input type="text" name="user_name" required pattern="^[\w -]{3,}$" title="3-50 chars: a-z, A-Z, 0-9, _, -" value="<?php echo($user_name ?? ''); ?>">
    <p>
        <h6>Email</h6>
        <input type="email" name="email" required value="<?php echo($email ?? ''); ?>">
        <h6>Confirm email</h6>
        <input type="email" name="email_confirm" required value="<?php echo($email_confirm ?? ''); ?>">
    </p>
    <p>
        <h6>Password</h6>
        <input type="password" name="password" required minlength="6" value="<?php echo($password ?? ''); ?>">
        <h6>Confirm password</h6>
        <input type="password" name="password_confirm" required minlength="6" value="<?php echo($password_confirm ?? ''); ?>">
    </p>
    <p>
        <input type="submit" value="create admin user">
    </p>
</form>
<style type="text/css">h1,form{text-align: center;} h6{margin: 6px 0 0 0;}</style>
