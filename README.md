# Project info
* css browser support: IE11+

## Windows installation
* follow this wsl guide https://bitbucket.org/swels/setup-webdev-wsl-windows-subsystem-for-linux  
* install python for gulp-sass `sudo apt-get install python`
* run in your workspace `git clone git@bitbucket.org:swels/web-stack.git`
  
## Run gulp to build frontend files  
```
nvm install  
npm install  
npm run gulp
```