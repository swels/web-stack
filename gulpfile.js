var gulp = require('gulp');
var fs = require('fs');
var conf = require('./gulpconfig.json');
var colors = require('colors');
var es = require('event-stream');
var path = require('path');
var filesExist = require('files-exist');
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins({
    rename: {
        'gulp-add-src': 'addsrc'
    }
});

//Shortcuts
var log = plugins.util.log;
var green = plugins.util.colors.green;
var cyan = plugins.util.colors.cyan;

//Arguments
var isProduction = process.argv.indexOf('--production') === -1 ? false : true;
var site = conf['swels'];

gulp.task('default', ['build', 'watch']);

gulp.task('build', ['scripts', 'styles', 'media', 'cachebust']);

gulp.task('watch', function() {
    for (var i in conf) {
        var site = conf[i];

        for (var j in site.scripts.js) {
            if(site.scripts.js[j].build.watch) {
                gulp.watch(site.scripts.js[j].build.watch, ['scripts', 'cachebust']);
            }
        }
        gulp.watch(site.styles.watch, ['styles', 'cachebust']);
    }
});

gulp.task('styles', function() {
    var stream = null;

    for (var i in conf) {
        var site = conf[i];

        // process stream for css
        var cssStream = gulp.src(site.styles.scss.main)
            .pipe(plugins.sass(isProduction ? {outputStyle: 'compressed'} : {}).on('error', plugins.sass.logError))
            .pipe(plugins.autoprefixer({browsers: [isProduction ? '> 0%' : 'last 2 versions']}));

        if (isProduction) {
            cssStream = cssStream.pipe(plugins.cssnano({zindex: false}));
        }

        cssStream = cssStream.pipe(gulp.dest(site.styles.target));

        log(green(isProduction ? 'Processed prod css' : 'Processed dev css') + ' of site \'' + cyan(i) + '\'');

        // merge css-stream into main-stream
        stream = (stream === null ? cssStream : es.merge(stream, cssStream));


        // process external libs stream
        var cssLibsStream = gulp.src(site.styles.css.libs);
        var scssLibsStream = gulp.src(site.styles.scss.libs)
            .pipe(plugins.sass(isProduction ? {outputStyle: 'compressed'} : {}).on('error', plugins.sass.logError))
            .pipe(plugins.autoprefixer({browsers: [isProduction ? '> 0%' : 'last 2 versions']}));
        
        scssLibsStream = es.merge(scssLibsStream, cssLibsStream).pipe(plugins.concat('libs.css'));
        
        if (isProduction) {
            scssLibsStream = scssLibsStream.pipe(plugins.cssnano({zindex: false}));
        }

        scssLibsStream = scssLibsStream.pipe(gulp.dest(site.styles.target));

        // merge libs-stream into main-stream
        stream = es.merge(stream, scssLibsStream);
    }

    return stream;
});

gulp.task('scripts', function() {
    var stream = null;

    for (var i in conf) {
        var site = conf[i];
        var siteStream = null;

        for (var j in site.scripts.js) {
            var jsStream = gulp.src(filesExist(site.scripts.js[j].res));

            jsStream = jsStream.pipe(plugins.babel({compact: false}));
                //.pipe(plugins.addsrc.prepend(filesExist(site.scripts.js[j].libs)));

            if (isProduction) {
                jsStream = jsStream.pipe(plugins.uglify({mangle: true}))
            }

            jsStream = jsStream
                .pipe(plugins.concat(site.scripts.js[j].build.name))
                .pipe(gulp.dest(site.scripts.js[j].build.dir));

            log(green(isProduction ? 'Processed prod js' : 'Processed dev js') + ' of site \'' + cyan(i) + '\'');

            siteStream = (siteStream === null ? jsStream : es.merge(siteStream, jsStream));
        }

        stream = (stream === null ? siteStream : es.merge(stream, siteStream));
    }

    return stream;
});

gulp.task('media', function() {
    var stream = null;

    for (var i in conf) {
        var site = conf[i];
        var siteStream = null;

        for(var mi in site.media) {
            var mediaType = site.media[mi];
            var mediaStream = null;

            for (var j in mediaType.res) {
                var fileStream = gulp.src(filesExist(mediaType.res[j])).pipe(gulp.dest(mediaType.dest.dir));
                mediaStream = (mediaStream === null ? fileStream : es.merge(mediaStream, fileStream));
            }

            siteStream = (siteStream === null ? mediaStream : es.merge(siteStream, mediaStream));
        }
        
        stream = (stream === null ? siteStream : es.merge(stream, siteStream));
    }

    return stream;
});

gulp.task('cachebust', function(cb) {
    fs.writeFile('cachebust.ini', ((new Date()).getTime()), cb);
});