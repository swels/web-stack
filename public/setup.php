<?php include_once($_SERVER['DOCUMENT_ROOT'].'/../runtime/index.php');

rt()->loginController->install();

// create admin user if no users exist
$dbResult = rt()->db()->query('SELECT id FROM `'.ENV['DB_NAME'].'`.`users` WHERE group_id=1');

if($dbResult && $dbResult->num_rows < 1) {
    rt()->template('header');

    if(!empty($_POST) && rt()->loginController->registerUser()) {
        echo('<h1 style="text-align: center;">Setup completed!</h1>');
    }
    else {
        rt()->template('setup', $_POST);
    }
    
    rt()->template('footer');
}
else {
    die('invalid request!');
}
